FROM python:3.6.5

RUN mkdir -p /usr/src/jkwiki

WORKDIR /tmp
COPY requirements.txt /tmp

RUN python3 -m venv /tmp/venv && chmod +x /tmp/venv/bin/activate
RUN . venv/bin/activate && pip3 install --no-cache-dir -r /tmp/requirements.txt

RUN apt-get update && apt-get install -y build-essential
RUN . venv/bin/activate && pip3 install --no-cache-dir uwsgi

EXPOSE 3031

WORKDIR /usr/src/jkwiki
ADD . /usr/src/jkwiki

# Run any mogrations and start uwsgi

CMD . /tmp/venv/bin/activate \
    && python3 ./migration.py db upgrade \
    && /tmp/venv/bin/uwsgi --plugins-dir /usr/lib/uwsgi/plugins/ \
              --plugin python34_plugin.so \
              --pp /tmp/venv/lib64/python3.6/site-packages/ \
              --pp /usr/src/jkwiki \
              -s 0.0.0.0:3031 \
              --file /usr/src/jkwiki/jkwiki.py \
              --callable app \
              -H /tmp/venv/