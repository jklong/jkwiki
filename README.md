# jkwiki

A personal, minimal, wiki-like system for notetaking in a linked, heirarchical structure with markdown style formatting.

Developed + tested in Chrome only. If something doesn't work you're welcome to submit a pull request.

## Dependencies
* Python 3.6.5
* Flask 1.0.2
* Flask-Markdown 0.3
* Flask-Migrate 2.1.1
* Flask-SQLAlchemy 2.3.2
* Flask-Script 2.0.6
* Markdown 2.6.11
* sqlalchemy 1.2.7
* psycopg2 2.7.4
* postgresql 10.4
* Bootstrap 4
* marked 0.3.19
* moment.js
* typeahead.js

## Usage

The included *Dockerfile*s and *docker-compose.yml* will build and start an environment with the website bound to localhost:80

`docker-compose up`

`docker-compose down`

Otherwise, use the included dev server - be sure to update `POSTGRES` dict in jkwiki.py with db server details and run any migrations.

`python migration.py db update`

`python jkwiki.py`

## TODOs in priority order
* ~~On-page preview for new/edit pages~~
* ~~Autocompletion of paths for page name/path field~~
* ~~Add route to handle auto sub-page creation. e.g. /p/\<parent\>/new -> /new with path prefilled~~
* ~~Add route to actually save edits~~
* Add and document minor jkwiki specific extensions to markdown syntax.
    * ~~Date insert/formatting - {{today}} -> 2018/5/22, {{now}} -> 2018/5/22 1429, {{rel: \<relativeDate\>}} - relative date/time specifier~~
    * Shortcut to page by path - {{/full/path/to/page}} or {{page}} if page is unique -> link to page in question
    * TOC and link generation based on headers and anchors. - {{toc:2-3}} -> Generate 2 level TOC for level 2 and 3 headers. {{toc: 1}} -> Generate TOC for level 1 headers
* Write a help article to have as the first page in the wiki
* Breadcrumb bar for parent pages
* Add page tags
* Support pgsql fulltext indexing for documents to facilitate search
* Change history/reversion
* HTTPS via nginx and LetsEncrypt
* Togglable autosave for edit/new

## Security Model

There is none. Seriously. No authentication for the API or frontend, no permissions, no user accounts, no auditing; this is fully open access for anyone who cares to look. If you want a security model, draft one and let me know. Then you can build it.

If you don't want people messing with your stuff, make sure to bind to the loopback only. Even then I wouldn't use this anywhere critical.