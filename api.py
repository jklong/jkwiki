''' Basic RESTful API for the interactive page functions '''

from jkwiki import app, db
from models import PageHeader, Page
from flask_sqlalchemy import SQLAlchemy
from flask.json import jsonify
from flask import abort, redirect
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound
import utils

@app.route('/api/_paths/')
def get_all_paths():
    all_paths = ['/'] + utils.get_paths()
    return jsonify(all_paths)

@app.route('/api/_paths/<path:path>/')
def get_paths(path):
    ''' Get all paths starting with <path> '''
    search_path = '/' + path #Tack on the root as it gets chomped
    
    results = list(map(lambda x: x.path + x.name ,PageHeader.query.filter((PageHeader.path + PageHeader.name).startswith(search_path)).all()))
    return jsonify(results)

@app.route('/api/_pages/delete/<path:full_path>/', methods=['DELETE'])
def delete_page(full_path):
    '''Delete the page corresponding to the given full path'''
    print(full_path)
    path, name = utils.split_path_and_name('/' + full_path)
    
    try:
        ph = PageHeader.query.filter(func.lower(PageHeader.path) == path.lower()).filter(func.lower(PageHeader.name) == name.lower()).one()
    
    except NoResultFound:
        abort(404)
    
    db.session.delete(ph.page)
    db.session.delete(ph)
    db.session.commit()

    return jsonify('success: True')