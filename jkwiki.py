from flask import Flask, app
from flaskext.markdown import Markdown
from models import db
import utils

POSTGRES = {
    'user': 'jkwiki',
    'pw': 'password',
    'db': 'jkwiki',
    'host': 'postgres',
    'port':'5432'
}

#Initialise App and db connection
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES
Markdown(app)

db.init_app(app)

#Pull in all our routes to register them via the decorators
from views import *

#Pull in API routes
from api import *

#Add in utility functions to our jinja context
app.jinja_env.globals.update(get_paths=utils.get_paths)
app.jinja_env.globals.update(is_current_path=utils.is_current_path)

def main():    
    app.run(debug=True)

if __name__ == '__main__':
    main()