from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from jkwiki import app, db


manager = Manager(app)
migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)

def main():
    manager.run()

if __name__ == '__main__':
    main()