from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, DateTime, String, Text, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects import postgresql

db = SQLAlchemy()

class PageHeader(db.Model):
    """Model for wiki page header information"""
    __tablename__ = 'page_headers'

    page_header_id = Column(Integer, primary_key=True)
    create_time = Column(DateTime)
    modify_time = Column(DateTime)
    path = Column(String, index=True)
    name = Column(String, index=True)
    page = relationship("Page", backref= db.backref('pageheader'), uselist=False)

    def get_path(self):
        return self.path + self.name

    def __repr__(self):
        return self.get_path()

class Page(db.Model):
    """Model for an actual page document"""
    __tablename__ = 'pages'

    page_id = Column(Integer, primary_key = True)
    page_header_id = Column(Integer, ForeignKey(PageHeader.page_header_id), index = True )
    text = Column(Text)

    def __repr__(self):
        return self.page_id + ':' + self.text[:10]