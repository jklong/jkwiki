''' Perform basic preprocessing of jkwiki-markdown extensions into standard markdown'''
from datetime import datetime
from dateutil.relativedelta import relativedelta
import re

def escaped_chars(text):
        '''Process escaped {} characters'''
        text = text.replace('\\{', '{')
        text = text.replace('\\}', '}')
        return text

class Preprocessor:

    _text = ''

    def __init__ (self, text):
        self._text = text

    def get_text(self):
        return self._text

    def _date_tags(self):
        ''' Find and replace for current date/datetime '''
        now   = datetime.now().strftime('%Y-%m-%d %H%M')
        today = datetime.now().strftime('%Y-%m-%d')

        self._text = self._text.replace('{{now}}', now)
        self._text = self._text.replace('{{today}}', today)

    def _parse_relative_date(self, spec):
        ''' Parse a relative date specifier and return a formatted date '''
        spec_regex = re.compile('([+-]?)(\d+)([a-z]+)')

        intervalMap = { # Map the various accepted representations of intervals 
                        # in the regex to something I can pass into a relativedelta constructor
            'd': 'days',
            'day': 'days',
            'days': 'days',
            'w': 'weeks',
            'wk': 'weeks',
            'week': 'weeks',
            'weeks': 'weeks',
            'm': 'minutes',
            'min': 'minutes',
            'mins': 'minutes',
            'h': 'hours',
            'hr': 'hours',
            'hour': 'hours',
            'hours': 'hours',
            'mo': 'months',
            'month': 'months',
            'months': 'months'
        }
        
        today = datetime.today()

        for match in re.finditer(spec_regex, spec):
            (op, amount, interval) = match.groups()

            if interval not in intervalMap:
                today = datetime.today()
                break

            today = today + relativedelta(**{intervalMap[interval]: int(op+amount)})
        
        return today.strftime('%Y-%m-%d')

    def _relative_date(self):
        ''' Find and process relative date tags '''
        relative_tag_regex = re.compile('{{rel:(.*?)}}')

        relative_tags = re.finditer(relative_tag_regex, self._text)

        for match in relative_tags:
            d = self._parse_relative_date(match.group(1))
            self._text = self._text.replace(match.group(0), d)

    def preprocess(self):

        self._date_tags()
        self._relative_date()
