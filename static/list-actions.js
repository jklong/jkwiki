console.log('list actions loaded');

function deleteFromList (e) {
    // Fire a request to the delete API for the page in this listitem. If ok animate a removal of the list item.
    baseURL = '/api/_pages/delete';
    path = baseURL + encodeURIComponent($(e.target).parent().prev().text()) + '/';

    //Call the delete function
    $.ajax(path, { method: 'DELETE', 
                    success: function() {
                            //Hide the parent li
                        $(e.target).parent().parent().slideUp();
                    }
                });

}

$(document).ready(function() {$('.icon-delete').on('click', deleteFromList)});