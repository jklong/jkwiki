console.log('md-render.js loaded');

function parseRelativeDate(spec) {
    //Parse a relative date specifier and return a formatted date as a string
    //Invalid specifiers simply return today.
    
    var specRegex = /([+-]?)(\d+)([a-z]+)/g; //Parse out the op, amount and interval

    var intervalMap = { //Map the various accepted representations of intervals 
                        //in the regex to something I can lookup and use for maths
        'd': 'd',
        'day': 'd',
        'days': 'd',
        'w': 'w',
        'wk': 'w',
        'week': 'w',
        'weeks': 'w',
        'm': 'm',
        'min': 'm',
        'mins': 'm',
        'h': 'h',
        'hr': 'h',
        'hour': 'h',
        'hours': 'h',
        'mo': 'M',
        'month': 'M',
        'months': 'M'
    };

    var today = moment();

    while(true) {
        var match = specRegex.exec(spec);
        if (match == null) {
            break;
        }

        var op = match[1];
        var amount = match[2];
        var interval = match[3];

        if(!(interval in intervalMap)) {
            today = moment();
            break;
        }

        if(op == '-'){
            amount *= -1;
        }

        today.add(amount, intervalMap[interval]);
    }

    return today.format('YYYY-MM-DD');
}

function jkwikiMDPreprocess(mdText) {
    // Preprocess the special extensions

    //Straight find-replace for now/today placeholders
    var today = moment().format('YYYY-MM-DD');
    var now = moment().format('YYYY-MM-DD HHmm');
    mdText = mdText.replace('{{today}}', today);
    mdText = mdText.replace('{{now}}', now);
    
    //loop through all relative date matches
    var relativeRegex = /{{rel:(.*?)}}/g; //Regex for a relative tag. Spec parsing is done in parseRelativeDate

    while(true) {
        var match = relativeRegex.exec(mdText);
        if (match == null) {
            break;
        }

        mdText = mdText.replace(match[0], parseRelativeDate(match[1]));
    }
    return mdText;
}

function renderMarkdown() {
    $('div#md-preview').html(marked(jkwikiMDPreprocess($('textarea#page-text').val())));
}

$(document).ready(function(){
    console.log('ready');
    $('input#utcOffset').val(moment().utcOffset());
    $('textarea').on('input', renderMarkdown);
    renderMarkdown();
});