console.log('path-typeahead.js loaded');
function slashTokeniser(str) {
    return str ? str.split(/\//) : []
}

$(document).ready(function(){
    var paths = new Bloodhound({
        datumTokenizer: slashTokeniser,
        queryTokenizer: slashTokeniser,
        prefetch: {url: '/api/_paths/'}
    });

    

    $('.path-typeahead').typeahead({minLength: 1,
         highlight: true,
         hint: true
        }, {
        name: 'paths',
        source: paths
    })
    
});