from models import PageHeader

def get_paths():
    ''' Generate a list of paths for nav from all layouts. In heirarchiacal-alphabetical order. '''
    headers = PageHeader.query.all()
    paths = list(map(lambda x: x.get_path(), headers))
    paths = sorted(paths, key=lambda x: x.lower())
    
    return paths

def split_path_and_name(full_path):
    ''' Split out the name and path components of a path for querying '''
    path_component = '/'.join(full_path.split('/')[:-1]) + '/'

    name_component = full_path.split('/')[-1:][0]

    return (path_component, name_component)

def is_current_path(full_path, url):
    if url[-1] == '/': url = url[:-1]
    full_path = full_path.lower()
    url = url.lower()
    return url.endswith(full_path)