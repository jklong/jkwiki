from jkwiki import app, db
from models import PageHeader, Page
from flask import render_template, abort, redirect
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm.exc import NoResultFound
from flask import g, request
from sqlalchemy import func
import utils
from datetime import datetime
from preprocessor import Preprocessor

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/p/<path:path>/')
def page(path):
    
    path, name = utils.split_path_and_name('/' + path)
 
    try:
        p = PageHeader.query.filter(func.lower(PageHeader.path) == path.lower()).filter(func.lower(PageHeader.name) == name.lower()).one()
    
    except NoResultFound:
        p = None
    
    if p is None:
        abort(404)
    
    g.page_title = p.name
    g.page_text = p.page.text
    return render_template('viewPage.html')

@app.route('/p/<path:full_path>/new')
@app.route('/new', methods=('GET', 'POST'))
def new_page(full_path = None):
    if request.method == 'GET':
        if full_path is not None:
            full_path = '/' + full_path + '/newPage'
        g.parent_path = full_path
        return render_template('new.html')

    ph = PageHeader()
    p = Page()
    
    path, name = utils.split_path_and_name(request.form['page-name'])

    pp = Preprocessor(request.form['page-text'])
    pp.preprocess()

    p.text = pp.get_text()
 
    if path[0] != '/':
        path = '/' + path

    ph.path = path
    ph.name = name
    ph.create_time = ph.modify_time = datetime.now()
    ph.page = p

    db.session.add_all((p, ph))
    db.session.commit()

    return redirect('/')
    
@app.route('/p/<path:full_path>/edit/', methods=('GET', 'POST'))
def edit_page(full_path):

    path, name = utils.split_path_and_name('/' + full_path)

    try:
        ph = PageHeader.query.filter(func.lower(PageHeader.path) == path.lower()).filter(func.lower(PageHeader.name) == name.lower()).one()
    
    except NoResultFound:
        ph = None

    if ph is None:
        abort(404)
    
    if request.method == 'GET':
        g.path = '/' + full_path
        g.text = ph.page.text
        return render_template('edit.html')
    
    path, name = utils.split_path_and_name(request.form['page-name'])
    ph.path = path
    ph.name = name
    ph.modify_time = datetime.now()
    pp = Preprocessor(request.form['page-text'])
    pp.preprocess()
    ph.page.text = pp.get_text()

    db.session.add(ph)
    db.session.commit()

    return redirect('/p/'+ path + name)
